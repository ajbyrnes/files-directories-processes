CC=		gcc
CFLAGS=		-g -gdwarf-2 -Wall -std=gnu99
LD=		gcc
LDFLAGS=	-L.
AR=		ar
ARFLAGS=	rcs
TARGETS=	search

all:		$(TARGETS)
#-------------------Intermediate Objects-------------------
execute.o: execute.c search.h
	@$(CC) $(CFLAGS) -c -o execute.o execute.c
	@echo Compiling execute.o...

filter.o: filter.c search.h
	@$(CC) $(CFLAGS) -c -o filter.o filter.c
	@echo Compiling filter.o...

main.o: main.c search.h
	@$(CC) $(CFLAGS) -c -o main.o main.c
	@echo Compiling main.o...

search.o: search.c search.h
	@$(CC) $(CFLAGS) -c -o search.o search.c
	@echo Compiling search.o...

utilities.o: utilities.c search.h
	@$(CC) $(CFLAGS) -c -o utilities.o utilities.c
	@echo Compiling utilities.o...
#---------------------EXECUTABLE-----------------------------
search: execute.o filter.o main.o search.o utilities.o
	@$(LD) $(LDFLAGS) -o search execute.o filter.o main.o search.o utilities.o
	@echo Linking search...
test:		search test_search.sh
	@echo Testing $<...
	@./test_search.sh

clean:
	@echo Cleaning...
	@rm -f $(TARGETS) *.o *.log *.input

.PHONY:		all test benchmark clean
