/* search.c */

#include "search.h"
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

/**
 * Recursively search through the provided root directory
 * @param   root        Path to directory
 * @param   settings    Settings structure
 * @return  Whether or not the search was successful.
 */
int search(const char *root, const Settings *settings) {
    DIR *dir;
    
//    int count = 0;
    //Initially opens the root path 
    dir = opendir(root); 
    
    //DEGUBGGING
    //printf("Opening: %s\n", root);

    if (dir == NULL) {
        fprintf(stderr, "Cannot open %s: %s\n", root, strerror(errno));
        return EXIT_FAILURE;
    }

    struct dirent * entry;
    //As long as there is something in the directory    
    while ((entry = readdir(dir)) != NULL) {
        if (streq(entry->d_name, "..")  || streq(entry->d_name,".")) 
          {  continue;
            }
        char path[BUFSIZ];
        sprintf(path, "%s/%s", root, entry->d_name);
        //print the path to the file
        
        if (!filter(path, settings)) { //if filter returns false
            execute(path, settings);
        }
        if (entry->d_type == DT_DIR) {
            //A directory
             //   printf("%s\n", path);
                search(path, settings); 
        }
    }


   closedir(dir);

return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
