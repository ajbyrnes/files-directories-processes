/* execute.c */
//this is a test 
#include "search.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <dirent.h>
#include <sys/wait.h>
#include <unistd.h>

/**
 * Executes the -print or -exec expressions (or both) on the specified path.
 * @param   path        Path to a file or directory
 * @param   settings    Settings structure.
 * @return  Whether or not the execution was successful.
 */
int execute(const char *path, const Settings *settings){
    // Execute -print
    if (settings->print){
        printf("%s\n", path);
    }

    // Execute -exec
   // int status = EXIT_SUCCESS;      // Status of execution
    if (settings->exec_argc > 0){
        // Create new child process from current parent
        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "Unable to Fork: %s\n", strerror(errno));
            _exit(EXIT_FAILURE);
        }
        
        char  * copy_args[settings->exec_argc + 1];
        
        if (pid == 0) { // Child
        
                for (int i = 0; i < settings->exec_argc; i++) {
                    if (!strcmp(settings->exec_argv[i], "{}")) {
                        copy_args[i] = (char *)path;
                       // printf("%s\n", copy_args[i]);

                } else {
                    copy_args[i] = settings->exec_argv[i];
                }
                }
                
                copy_args[settings->exec_argc] = NULL;

                if (execvp(copy_args[0], copy_args) < 0){
                    fprintf(stderr, "Unable to exec: %s\n", strerror(errno));
                }

                _exit(EXIT_FAILURE);
            } else  { //Parent
                int status;
                while (wait(&status) != pid){
                }
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
