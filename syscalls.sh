#!/bin/sh
# syscalls.sh
# Summarize system calls used by search and find

echo "./search /etc"
strace ./search /etc 2>&1 | grep -Eo ".*\(" | cut -d '(' -f1 | sort | uniq -c | sort -nr

echo "find /etc"
strace find /etc 2>&1 | grep -Eo ".*\(" | cut -d '(' -f1 | sort | uniq -c | sort -nr
