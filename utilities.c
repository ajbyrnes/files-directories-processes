/* utilities.c */

#include "search.h"
#include<stdio.h>
#include<stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include<stdbool.h>
#include <dirent.h>
#include <sys/stat.h>
#include<sys/types.h>
#include <unistd.h>

/**
 * Test whether or not a directory is empty.
 * @param   path        Path to directory.
 * @return  Whether or not a directory is empty.
 */
bool        is_directory_empty(const char *path) {
    struct dirent * entry;
    DIR * dir = opendir(path);
    
    if (dir == NULL)  //Does not Exist/Not a Directory
        //Print our error message
       { //fprintf(stderr, "Unable to open: %s: %s\n", path, strerror(errno));
            return false;
   }  
   bool empty = true;
    // Count the number of files in directory 
    while ((entry = readdir(dir)) != NULL) {
        if ( !streq(entry->d_name, ".") && !streq(entry->d_name, "..")) empty = false;
    }
    
    
    closedir(dir); //close the directory
    return empty;
}

/**
 * Retrieve the modification time of the given file.
 * @param   path        Path to file of directory.
 * @return  The modification time of the given file.
 */
time_t      get_mtime(const char *path) {
    //Should probably check if file is valid
    struct stat info;
    stat(path, &info); 
    //if (stat(path, &info) < 0) return -1;
    //prints out last modified time for debugging purposes
    //printf("Last modified time: %s\n", ctime(&info.st_mtime));
    return info.st_mtime;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
